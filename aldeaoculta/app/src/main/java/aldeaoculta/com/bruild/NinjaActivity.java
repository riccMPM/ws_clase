package aldeaoculta.com.bruild;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import aldeaoculta.com.bruild.adapter.NinjaAdapter;
import aldeaoculta.com.bruild.model.Ninja;

public class NinjaActivity extends AppCompatActivity {

    private ImageView iviNinja;
    private TextView tviTitulo, tviDescripcion;
    private WebView wviFacebook;
    Ninja ninja;

    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ninja);

        iviNinja = findViewById(R.id.iviNinja);
        tviTitulo = findViewById(R.id.tviTitulo);
        tviDescripcion = findViewById(R.id.tviDescripcion);
        wviFacebook = findViewById(R.id.wviFacebook);

        ninja = (Ninja) getIntent().getSerializableExtra("intNinja");


        setTitle(ninja.getRangoNinja());

        iviNinja.setImageDrawable(ContextCompat.getDrawable(this, ninja.getImagen()));
        tviTitulo.setText(ninja.getRangoNinja());
        tviDescripcion.setText(ninja.getDescripcion());

        wviFacebook.getSettings().setJavaScriptEnabled(true);
        wviFacebook.setWebViewClient(new WebViewClient());
        wviFacebook.loadUrl("https://www.facebook.com/AcademiaDeLaTecnologia/");
    }



}
