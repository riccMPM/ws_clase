package aldeaoculta.com.bruild;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import aldeaoculta.com.bruild.adapter.NinjaAdapter;
import aldeaoculta.com.bruild.model.Ninja;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rviNinjas;
    List<Ninja> lstNinja;
    NinjaAdapter adapter;
    //Comentario  Git
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rviNinjas = findViewById(R.id.rviNinjas);

        mostrarNinjas();
    }


    private void mostrarNinjas() {
        lstNinja = new ArrayList<>();

        lstNinja.add(new Ninja("Hokage","Es el líder supremo de Konohagakure. Por lo general, es el shinobi más fuerte en el pueblo, aunque la ideología y renombre juega un papel importante para ser elegido para el puesto. Seis shinobis y una kunoichi han ganado este título hasta ahora.",R.drawable.hokage,"27","Culminar el examen Chunin", 5));
        lstNinja.add(new Ninja("Shinobi","Son el foco primario y el principal poder militar en la serie. Un ninja hombre es llamada shinobi. La mayoría proviene de una Aldea Oculta, algunos de clanes ninja especiales de dichas aldeas y llevarán a cabo misiones por un costo (ryos). ",R.drawable.shinobi,"20","Ninguno",1));
        lstNinja.add(new Ninja("Kunoichi","Son el foco primario y el principal poder militar en la serie. Un ninja mujer es llamada kunoichi. La mayoría proviene de una Aldea Oculta, algunos de clanes ninja especiales de dichas aldeas y llevarán a cabo misiones por un costo (ryos). ",R.drawable.kunoichi,"20","Ninguno",1));

        adapter = new NinjaAdapter(this, lstNinja);
        layoutManager = new LinearLayoutManager(this);

        rviNinjas.setHasFixedSize(true);
        rviNinjas.setLayoutManager(layoutManager);
        rviNinjas.setAdapter(adapter);
    }
}
