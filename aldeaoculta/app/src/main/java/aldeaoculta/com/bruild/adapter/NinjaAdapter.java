package aldeaoculta.com.bruild.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;



import java.util.List;

import aldeaoculta.com.bruild.NinjaActivity;
import aldeaoculta.com.bruild.R;
import aldeaoculta.com.bruild.model.Ninja;


/**
 * Created by Lenovo on 16/01/2017.
 */

public class NinjaAdapter extends RecyclerView.Adapter<NinjaAdapter.ViewHolder> {


    private Context mContext;
    private List<Ninja> lstNinja;

    public NinjaAdapter(Context context, List<Ninja> lstNinja) {
        this.mContext = context;
        this.lstNinja = lstNinja;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ninja, parent, false);

        return new ViewHolder(v, mContext);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        final Ninja ninja = lstNinja.get(position);
        viewHolder.tviTitulo.setText(ninja.getRangoNinja());
        viewHolder.tviDescripcion.setText(ninja.getDescripcion());


        viewHolder.iviFoto.setImageDrawable(ContextCompat.getDrawable(mContext, ninja.getImagen()));
        viewHolder.rbaDificultad.setRating(ninja.getDificultad());

        viewHolder.ninja = ninja;



    }


    @Override
    public int getItemCount() {
        return this.lstNinja.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tviTitulo,tviDescripcion;
        RatingBar rbaDificultad;
        ImageView iviFoto;
        Context context;
        Ninja ninja;


        public ViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            tviTitulo = view.findViewById(R.id.tviTitutlo);
            tviDescripcion = view.findViewById(R.id.tviDescripcion);
            rbaDificultad = view.findViewById(R.id.rbaDificultad);
            iviFoto = view.findViewById(R.id.iviFoto);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), NinjaActivity.class);
            intent.putExtra("intNinja", ninja);
            v.getContext().startActivity(intent);

        }
    }


}