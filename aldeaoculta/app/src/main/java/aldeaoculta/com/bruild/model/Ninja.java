package aldeaoculta.com.bruild.model;

import java.io.Serializable;

public class Ninja implements Serializable{

    private String rangoNinja;
    private String descripcion;
    private int imagen;
    private String randoEdad;
    private String requisito;
    private  int dificultad;

    public Ninja(String rango, String descripcion, int imagen, String randoEdad, String requisito, int dificultad ) {
        this.rangoNinja = rango;
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.randoEdad = randoEdad;
        this.requisito = requisito;
        this.dificultad = dificultad;
    }

    public String getRangoNinja() {
        return rangoNinja;
    }

    public void setRangoNinja(String rango) {
        this.rangoNinja = rango;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getRandoEdad() {
        return randoEdad;
    }

    public void setRandoEdad(String randoEdad) {
        this.randoEdad = randoEdad;
    }

    public String getRequisito() {
        return requisito;
    }

    public void setRequisito(String requisito) {
        this.requisito = requisito;
    }

    public int getDificultad() {
        return dificultad;
    }

    public void setDificultad(int dificultad) {
        this.dificultad = dificultad;
    }
}
